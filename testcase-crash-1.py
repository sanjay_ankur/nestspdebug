#!/usr/bin/env python
"""
Test sim to reproduce bug.

File: testSim.py
"""
import nest

nest.ResetKernel()
nest.set_verbosity('M_ALL')
nest.SetKernelStatus(
    {
        'resolution': 0.001,
        'local_num_threads': 1
    }
)
# Update the SP interval
nest.EnableStructuralPlasticity()
nest.SetStructuralPlasticityStatus({
    'structural_plasticity_update_interval':
    10000000,
})

growth_curve = {
    'growth_curve': "gaussian",
    'growth_rate': 0.0001,  # Beta (elements/ms)
    'continuous': True,
    'eta': 0.1,
    'eps': 0.7,
}
structural_p_elements_E = {
    'Den_ex': growth_curve,
    'Den_in': growth_curve,
    'Axon_ex': growth_curve
}
neuronDict = {'V_m': -60.,
              't_ref': 5.0, 'V_reset': -60.,
              'V_th': -50., 'C_m': 200.,
              'E_L': -60., 'g_L': 10.,
              'E_ex': 0., 'E_in': -80.,
              'tau_syn_ex': 5., 'tau_syn_in': 10.,
              'I_e': 200.}

nest.SetDefaults("iaf_cond_exp", neuronDict)
neuronsE = nest.Create('iaf_cond_exp', 1, {
    'synaptic_elements': structural_p_elements_E})

# synapses
synDictE = {'model': 'static_synapse',
            'weight': 3.,
            'pre_synaptic_element': 'Axon_ex',
            'post_synaptic_element': 'Den_ex'}

nest.SetStructuralPlasticityStatus({
    'structural_plasticity_synapses': {
        'synapseEE': synDictE,
    }
})

nest.Simulate(200*1000)
