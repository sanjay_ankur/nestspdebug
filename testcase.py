#!/usr/bin/env python
"""
Test sim to reproduce bug.

File: testSim.py
"""

from __future__ import print_function
import sys
sys.argv.append('--quiet')
import nest
import random


class testSim:

    """Test sim."""

    def __init__(self):
        """Initialise variables."""
        # default resolution in nest is 0.1ms. Using the same value
        # http://www.nest-simulator.org/scheduling-and-simulation-flow/
        self.dt = 0.1

        # structural plasticity bits
        # must be an integer
        self.sp_update_interval = 100  # ms

        self.rank = nest.Rank()

        self.weightEE = 3.
        self.weightII = -30.
        self.weightEI = 3.
        self.weightExtE = 50.
        self.weightExtI = 50.

        random.seed(42)

    def __setup_neurons(self):
        """Setup properties of neurons."""
        self.growth_curve_axonal_E = {
            'growth_curve': "gaussian",
            'growth_rate': 0.0001,  # Beta (elements/ms)
            'continuous': False,
            'eta': 0.1,
            'eps': 0.7,
        }
        self.growth_curve_axonal_I = {
            'growth_curve': "gaussian",
            'growth_rate': 0.0001,  # Beta (elements/ms)
            'continuous': False,
            'eta': 0.1,
            'eps': 0.7,
        }
        self.growth_curve_dendritic_E = {
            'growth_curve': "gaussian",
            'growth_rate': 0.0001,  # Beta (elements/ms)
            'continuous': False,
            'eta': 0.1,
            'eps': 0.7,
        }
        self.growth_curve_dendritic_I = {
            'growth_curve': "gaussian",
            'growth_rate': 0.0001,  # Beta (elements/ms)
            'continuous': False,
            'eta': 0.1,
            'eps': 0.7,
        }

        self.structural_p_elements_E = {
            'Den_ex': self.growth_curve_dendritic_E,
            'Den_in': self.growth_curve_dendritic_I,
            'Axon_ex': self.growth_curve_axonal_E
        }

        self.structural_p_elements_I = {
            'Den_ex': self.growth_curve_dendritic_I,
            'Den_in': self.growth_curve_dendritic_I,
            'Axon_in': self.growth_curve_axonal_I
        }

        # see the aif source for symbol definitions
        self.neuronDict = {'V_m': -60.,
                           't_ref': 5.0, 'V_reset': -60.,
                           'V_th': -50., 'C_m': 200.,
                           'E_L': -60., 'g_L': 10.,
                           'E_ex': 0., 'E_in': -80.,
                           'tau_syn_ex': 5., 'tau_syn_in': 10.}

        nest.CopyModel("iaf_cond_exp", "tif_neuron")
        nest.SetDefaults("tif_neuron", self.neuronDict)

        # external current
        self.poissonExtDict = {'rate': 10., 'origin': 0., 'start': 0.}

    def __create_neurons(self):
        """Create our neurons."""
        self.neuronsE = nest.Create('tif_neuron', self.populations['E'], {
            'synaptic_elements': self.structural_p_elements_E})
        self.neuronsI = nest.Create('tif_neuron', self.populations['I'], {
            'synaptic_elements': self.structural_p_elements_I})

        self.poissonExtE = nest.Create('poisson_generator',
                                       self.populations['Poisson'],
                                       params=self.poissonExtDict)
        self.poissonExtI = nest.Create('poisson_generator',
                                       self.populations['Poisson'],
                                       params=self.poissonExtDict)

    def __setup_connections(self):
        """Setup connections."""
        self.connectionNumberExtE = 1
        self.connectionNumberExtI = 1

        # connection dictionaries
        self.connDictExtE = {'rule': 'fixed_indegree',
                             'indegree': self.connectionNumberExtE}
        self.connDictExtI = {'rule': 'fixed_indegree',
                             'indegree': self.connectionNumberExtI}

        self.synDictEE = {'model': 'static_synapse',
                          'weight': self.weightEE,
                          'pre_synaptic_element': 'Axon_ex',
                          'post_synaptic_element': 'Den_ex'}

        self.synDictEI = {'model': 'static_synapse',
                          'weight': self.weightEI,
                          'pre_synaptic_element': 'Axon_ex',
                          'post_synaptic_element': 'Den_ex'}

        self.synDictII = {'model': 'static_synapse',
                          'weight': self.weightII,
                          'pre_synaptic_element': 'Axon_in',
                          'post_synaptic_element': 'Den_in'}

        self.synDictIE = {'model': 'static_synapse',
                          'weight': self.weightII,
                          'pre_synaptic_element': 'Axon_in',
                          'post_synaptic_element': 'Den_in'}

        nest.SetStructuralPlasticityStatus({
            'structural_plasticity_synapses': {
                'synapseEE': self.synDictEE,
                'synapseEI': self.synDictEI,
                'synapseII': self.synDictII,
                'synapseIE': self.synDictIE,
            }
        })

    def __connect_stim(self):
        """Connect the neuron sets up."""
        nest.Connect(self.poissonExtE, self.neuronsE,
                     conn_spec=self.connDictExtE,
                     syn_spec={'model': 'static_synapse',
                               'weight': self.weightExtE})
        nest.Connect(self.poissonExtI, self.neuronsI,
                     conn_spec=self.connDictExtI,
                     syn_spec={'model': 'static_synapse',
                               'weight': self.weightExtI})

    def __setup_detectors(self):
        """Setup spike detectors."""
        self.sdE = nest.Create('spike_detector')
        self.sdI = nest.Create('spike_detector')
        nest.Connect(self.neuronsE, self.sdE)
        nest.Connect(self.neuronsI, self.sdI)

    def setup_test_simulation(self):
        """Set up a test simulation."""
        # Nest stuff
        nest.ResetKernel()
        # http://www.nest-simulator.org/sli/setverbosity/
        nest.set_verbosity('M_DEBUG')
        nest.SetKernelStatus(
            {
                'resolution': self.dt,
                'local_num_threads': 1
            }
        )
        # Update the SP interval
        nest.EnableStructuralPlasticity()
        nest.SetStructuralPlasticityStatus({
            'structural_plasticity_update_interval':
            self.sp_update_interval,
        })

        # a much smaller simulation
        self.populations = {'E': 80, 'I': 20, 'Poisson': 1}

        self.__setup_neurons()
        self.__create_neurons()
        self.__setup_connections()
        self.__connect_stim()
        self.__setup_detectors()

    def run_simulation(self, simtime=200):
        """Run the simulation."""
        nest.Simulate(simtime*1000)

if __name__ == "__main__":
    simulation = testSim()
    simulation.setup_test_simulation()
    simulation.run_simulation()
